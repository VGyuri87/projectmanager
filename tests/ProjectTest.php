<?php

use App\Http\Logic\ProjectManagement;
use App\Http\Structures\ProjectStucture;
use PHPUnit\Framework\TestCase;

class ProjectTest extends TestCase
{

    /** @var ProjectManagement $projectManagement */
    public $projectManagement;

    public function setUp()
    {
        $this->projectManagement = new ProjectManagement();
    }

    public function provideDataHourToDay()
    {
        return [
            'test 1' => [
                'developmentTime' => '3',
                'finish' => '0',
            ],
            'test 2' => [
                'developmentTime' => '16',
                'finish' => '2',
            ],
            'test 3' => [
                'developmentTime' => '6',
                'finish' => '0',
            ],
            'test 4' => [
                'developmentTime' => '8',
                'finish' => '0',
            ],
        ];
    }

    /**
     * @dataProvider provideDataHourToDay
     * @param $start
     * @param $developmentTime
     * @param $finish
     */
    public function testHourToDay($developmentTime, $finish)
    {
        $projectStructure = new ProjectStucture();
        $projectStructure->developmentTime = $developmentTime;

        $this->assertEquals($finish, $this->projectManagement->getDayFromWorkHour($projectStructure->developmentTime));
    }

    public function provideDataToWorkingHour()
    {
        return [
            'in working hour in weekend' => [
                'start' => '2018-02-03 12:34:22',
                'isWorkingHour' => false,
            ],
            'not working hour in weekend' => [
                'start' => '2018-02-03 18:04:12',
                'isWorkingHour' => false,
            ],
            'in working hour in week' => [
                'start' => '2018-02-01 10:12:34',
                'isWorkingHour' => true,
            ],
            'not working hour in week' => [
                'start' => '2018-02-01 08:12:34',
                'isWorkingHour' => false,
            ],
        ];
    }

    /**
     * @dataProvider provideDataToWorkingHour
     */
    public function testWorkingHour($start, $isWorkingHour)
    {
        $result = $this->projectManagement->isWorkingHour($start);
        $this->assertEquals($isWorkingHour, $result);
    }

    public function provideDataToCalculateWorkTime()
    {
        return [
            'in working hour in weekend' => [
                'start' => '2018-02-03 12:34:22',
                'developmentTime' => '1',
                'finish' => 'The project finish at 2018.2.3 (out of work time)',
            ],
            'not working hour in weekend' => [
                'start' => '2018-02-03 18:04:12',
                'developmentTime' => '12',
                'finish' => 'The project finish at 2018.2.5 (out of work time)',
            ],
            'in working hour in week' => [
                'start' => '2018-02-01 10:12:34',
                'developmentTime' => '8',
                'finish' => 'The project finish at 2018.2.1 (in work time)',
            ],
            'not working hour in week' => [
                'start' => '2018-02-01 08:12:34',
                'developmentTime' => '21',
                'finish' => 'The project finish at 2018.2.4 (out of work time)',
            ],
        ];
    }

    /**
     * @dataProvider provideDataToCalculateWorkTime
     */
    public function testFinishDay($start, $developmentTime, $finish)
    {
        $project = new ProjectStucture();
        $project->startDate = $start;
        $project->developmentTime = $developmentTime;
        $result = $this->projectManagement->calculateFinish($project);
        $this->assertEquals($finish, $result);
    }
}