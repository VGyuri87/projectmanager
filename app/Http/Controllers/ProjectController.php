<?php

namespace App\Http\Controllers;


use App\Http\Logic\ProjectManagement;
use App\Http\Structures\ProjectStucture;

class ProjectController
{

    public function index()
    {
        return view('setProjectView');
    }

    public function calculate()
    {
        $project = new ProjectStucture();
        $projectManager = new ProjectManagement();
        if ($_REQUEST) {
            $project->startDate = $_REQUEST['projectStart'];
            $project->developmentTime = $_REQUEST['developTime'];

            $projectManager->calculateFinish($project);
        }
        $data = [
            'finish' => $projectManager->getFinishDate(),
        ];
        return view('setProjectView', $data);
    }
}