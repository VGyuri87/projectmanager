<?php

namespace App\Http\Logic;

use App\Http\Structures\ProjectStucture;
use Carbon\Carbon;

class ProjectManagement
{
    /** @var string $finishDate */
    public $finishDate;
    /** @var Carbon $carbon */
    public $carbon;
    /** @var int WORK_TIME_START */
    const WORK_TIME_START = 9;
    /** @var int WORK_TIME_END */
    const WORK_TIME_END = 17;

    public function calculateFinish(ProjectStucture $projectStructure)
    {
        $this->carbon = new Carbon($projectStructure->startDate);
        $this->setFinishDate($projectStructure->developmentTime);
        if ($this->isWorkingHour($projectStructure->startDate)) {
            return 'The project finish at ' . $this->getFinishDate() . ' (in work time)';
        } else {
            return 'The project finish at ' . $this->getFinishDate() . ' (out of work time)';
        }
    }

    /**
     * @param string $date
     * @return bool
     */
    public function isWorkingHour($date = '')
    {
        if ($this->carbon == null) {
            $this->carbon = Carbon::parse($date);
        }

        if ($this->carbon->isWeekend()) {
            return false;
        }

        return $this->carbon->hour > self::WORK_TIME_START && $this->carbon->hour < self::WORK_TIME_END;
    }

    /**
     * @param int $developmentTime
     */
    private function setFinishDate($developmentTime)
    {
        $days = $this->getDayFromWorkHour($developmentTime);
        $this->carbon->addDays($days);
        $this->finishDate = $this->carbon->year . '.' . $this->carbon->month . '.' . $this->carbon->day;
    }

    /**
     * @return string
     */
    public function getFinishDate()
    {
        return $this->finishDate;
    }

    /**
     * @param int $developTime
     * @return int
     */
    public function getDayFromWorkHour($developTime)
    {
        if ($developTime <= 8 && $developTime > 0) {
            return 0;
        }

        return (int)round($developTime / 8, 0);
    }
}